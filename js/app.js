
var app = angular.module('deliveryApp', ["ngTable", "moment-picker"]);

app.controller('DeliveryController', function ($window, NgTableParams) {

    this.$inject = ["NgTableParams"];
    $('.carousel.carousel-slider').carousel({
        padding:50,
        full_width: true
    });
    $('.modal').modal({ complete: function () { $('.carousel').carousel('set', 0); } });

    var self = this;

    self.cols = [
      { field: "nombre", sortable: "nombre", sortDirection: "desc" },
      { field: "direccion", sortable: "direccion", sortDirection: "asc" },
      { field: "telefono", sortable: "telefono", sortDirection: "asc" }
    ];

    var initialParams = {
        count: 2, // initial page size
        sorting: { nombre: "asc" }
    };

    var initialSettings = {
        // page size buttons (right set of buttons in demo)
        counts: [],
        // determines the pager buttons (left set of buttons in demo)
        paginationMaxBlocks: 8,
        paginationMinBlocks: 2,
        dataset: list
    };

    this.applyNameSearch = function () {
        var term = self.searchNombre;
        self.tableParams.filter({ nombre: term });
    }

    this.applyDireccionSearch = function () {
        var term = self.searchDireccion;
        self.tableParams.filter({ direccion: term });
    }

    self.tableParams = new NgTableParams(initialParams, initialSettings);
  
    this.delivery = {};
    this.deliveryAux = {};
    this.deliveries = list; //[];
    this.descripcionMaxLenght = 1000;
    this.especialidadesMaxLenght = 500;
    this.editMode = false;
    this.horarioApertura = '';
    this.horarioCierre = '';

    this.searchNombre = '';
    this.searchDireccion = '';
    this.height = $window.innerHeight - angular.element('nav').height() - angular.element('footer').height() - 40;
    this.sortType = 'nombre'; 
    this.sortReverse = false;
    this.searchDelivery = '';
    this.submited = false;
    this.ErrorTelefono = false;
    this.ErrorDireccion = false;

    angular.element($window).bind('resize', function () {
        console.log('resize');
        this.height = $window.innerHeight - angular.element('nav').height() - angular.element('footer').height();

    });


    this.checkRequiredField = function (input) {
        console.log(input);
        if (angular.element("#" + input).hasClass('ng-invalid')) {
            

        } 
    }

    this.checkForm = function () {

    }

    this.deliverySelected = function () {
        if (this.delivery != null) {
            return ' active';
        } else {
            return '';
        }
    }

    this.addDelivery = function () {
        this.deliveries.push(this.delivery);

        this.submited = true;

        this.delivery = {};

        this.deliveryForm.$setUntouched();


        //angular.element(".error-message").hide();

        angular.element("form, input").removeClass("ng-touched");
        angular.element("form, textarea").removeClass("ng-touched");
        angular.element("form, input").removeClass("valid");
        angular.element("form, textarea").removeClass("valid");
        this.refreshFields();
        this.editMode == false;
        //show confirmation message and go back to list
        $('#modal1').modal('open');
        

        console.log(this.deliveries);
    };

    this.edit = function (item) {
        this.delivery = item;
        this.editMode = true;

        var index = this.deliveries.indexOf(item);
        this.deliveries.splice(index, 1);

        console.log(this.deliveries);

        this.refreshFields();
        $('.carousel').carousel('set', 1);
        this.submited = false;
    };

    this.delete = function (item) {
        $('#modal2').modal('open');
        this.delivery = item;
    };

    this.deleteConfirmed = function () {
        var index = this.deliveries.indexOf(this.delivery);
        this.deliveries.splice(index, 1);
        this.delivery = {};
    };

    this.deleteCanceled = function () {
        this.delivery = {};
        $('#modal2').modal('close');
    };


    this.cancel = function () {
        if (this.editMode == true) {
            this.deliveries.push(this.delivery)
            this.editMode = false;
        }

        console.log('cancel');
        console.log(this.deliveries);

        this.delivery = {};
        this.refreshFields();
        $('.carousel').carousel('set', 0);
        $window.pageYOffset;
        //scroll top here
    };

    this.newDelivery = function () {
        this.delivery = {};
        this.refreshFields();
        $('.carousel').carousel('set', 1);
    };

    this.refreshFields = function () {
        //add a loader, and show data only when loaded
        //var interval = setInterval(function () {
        //}, 3000);

        //function myStopFunction() {
        //    clearInterval(myVar);
        //}

        setTimeout(function () {
            Materialize.updateTextFields();
            console.log('updatedfields');
        }, 10);

    };

    

});

app.directive('deliveryForm', function () {
    return {
        restrict: 'E',
        templateUrl: 'delivery-form.html'
    };
});

app.directive('deliveryList', function () {
    return {
        restrict: 'E',
        templateUrl: 'delivery-list.html'
    };
});


//app.controller('TabController', function () {
//    this.tab = 1;

//    this.setTab = function (newValue) {
//        this.tab = newValue;
//    };

//    this.isSet = function (tabName) {
//        return this.tab === tabName;
//    };
//});




var list = [
{
    nombre: 'bbbb',
    direccion: 'adsf 324',
    telefono: '32548565',
    contacto_administrativo: {
        nombre: 'juan',
        apellido: 'bdsf 324',
        telefono: '32548565',
        email: 'juan@ads.com',
    },
    contacto_comercial: {
        nombre: 'juan comercial',
        apellido: 'adsf 324',
        telefono: '32548565',
        email: 'juan@ads.com',
        idemContactoAdministrativo: false,
    }

},
{
    nombre: 'aaaaa',
    direccion: 'cdsf 324',
    telefono: '12548565'
},
{
    nombre: 'ccccc',
    direccion: 'adsf 324',
    telefono: '42548565'
}
];